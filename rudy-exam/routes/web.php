<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\ProductController;


Route::get('/', [LoginController::class, 'index'])->name('login');
Route::get('login', [LoginController::class, 'index']);
Route::get('register', [RegisterController::class, 'index'])->name('register');

Route::post('post-login', [LoginController::class, 'login'])->name('login.post'); 
Route::get('logout', [LoginController::class, 'logout'])->name('logout');

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/dashboard', [ProductController::class, 'dashboard'])->name('dashboard');
     
});