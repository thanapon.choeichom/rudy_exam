<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Resources\Product as ProductResource;
   
class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::join('shops','products.shop_id','=','shops.id')
        ->selectRaw(
            'products.id,
            products.name,
            products.shop_id,
            products.status,
            products.image,
            DATE_FORMAT(products.created_at , "%d%/%m%/%Y %T") as created_date,
            DATE_FORMAT(products.updated_at , "%d%/%m%/%Y %T") as updated_date,
            FORMAT(products.price, 2) as price,
            FORMAT(products.discount, 2) as discount,
            FORMAT(products.vatprice, 2) as vatprice,
            FORMAT(products.totalprice, 2) as totalprice,
            shops.name as shop_name',
            )
        ->get();
        return $this->sendResponse($products, 'Get Products successfully.',null);
    }

    public function dashboard()
    {
        if(Auth::check()){
            return view('dashboard');
        }else{
            return redirect("login")->withSuccess('Opps! You do not have access');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'name' => 'required',
            'status' => 'required',
            'shop_id' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif,svg',
            'price' => 'required',
            'discount'=>'required',
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $product = new Product;
        $product->name = $input['name'];
        $product->status = $input['status'];
        $product->shop_id = $input['shop_id'];


        $product->price = $input['price'];
        $product->discount = $input['discount'];

        $total = $input['price']-$input['discount'];

        $product->vat = 7;

        $vatprice = ($input['price']-$input['discount'])*7/100;

        $product->vatprice =  $vatprice;

        $product->totalprice = $total+$vatprice;
        if($request->file('image')!=null) {
            $name = $request->file('image')->getClientOriginalName();
            $path = $request->image->move(public_path('uploads'), $name);
            $product->image = $name;
           
        }
        $product->save();
   
        return $this->sendResponse(new ProductResource($product), 'Add Product successfully.',null);
    } 
   
    public function uploadImage(Request $request)
    {
         
        $validatedData = $request->validate([
         'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
 
        ]);
 
        $name = $request->file('image')->getClientOriginalName();
 
        // $path = $request->file('image')->storeAs('public/images',$name);
        $path = $request->image->move(public_path('uploads'), $name);
        return redirect('upload-image')->with('status', 'Image Has been uploaded');
 
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
  
        if (is_null($product)) {
            return $this->sendError('Product not found.');
        }
   
        return $this->sendResponse(new ProductResource($product), 'Product successfully.',null);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        $product = Product::find($id);
        $validator = Validator::make($input, [
            'name' => 'required',
            'status' => 'required',
            'shop_id' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $product->name = $input['name'];
        $product->status = $input['status'];
        $product->shop_id = $input['shop_id'];
        $product->update();
   
        return $this->sendResponse(new ProductResource($product), 'Update Product successfully.',null);
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
   
        return $this->sendResponse([], 'Product deleted successfully.',null);
    }

    public function getShop()
    {
        $shops = Shop::get();
        return $this->sendResponse($shops, 'Get Products successfully.',null);
    }

    public function preventSQLInject(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, [
            'data' => 'integer',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $products = DB::table(
            'products'
            )
            ->select('products.*')
            ->whereRaw('products.shop_id = ?', $input['data'])
        ->get();
    
        return $this->sendResponse($products, 'Get Products successfully.',null);
    }
}