import { createApp } from 'vue';
require('./bootstrap');
let app=createApp({})
app.component('product-component', require('../components/ProductComponent.vue').default);
app.component('login-component', require('../components/LoginComponent.vue').default);
app.component('register-component', require('../components/RegisterComponent.vue').default);
app.mount("#app")