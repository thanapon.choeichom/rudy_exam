<!DOCTYPE html>
<html>
  
<head>
    <title>Prevent SQL Injection</title>
    <link rel="stylesheet" type="text/css" 
        href="style.css">
</head>
  
<body>
    <div id="form">
        <h1>SQL INJECTION</h1>
        <form name="form" 
            action="verifyQuery.php" method="POST">
  
            <p>
                <label> shop_id: </label>
                <input type="text" id="shop_id" 
                    name="shop_id" />
            </p>
  
            <p>
                <input type="submit" 
                    id="button" value="Run" />
            </p>
        </form>
    </div>
</body>
  
</html>