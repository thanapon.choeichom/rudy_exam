# Rudy_Exam




ส่วนที่ 1
สร้าง Project ที่ใช้ laravel 
 - api สามารถ Login ได้ rudy-exam\app\Http\Controllers\API\LoginController.php
 - api สามารถ สมัครสมาชิกได้ rudy-exam\app\Http\Controllers\API\RegisterController.php
 - api สามารถ อับรูปภาพได้ rudy-exam\app\Http\Controllers\API\ProductController.php
 - ใช้ laravel access token api rudy-exam\routes\api.php

--> Folder "rudy-exam" => สร้างโดย Laravel + Vue 3


ส่วนที่ 2
1. ให้อธิบายว่าจะใช้วิธีการอะไรได้บ้างในการป้องกัน brute
force attack หรือเดารหัสผ่านใน login form

--> ใช้ Login Attemp เช็คการ Login หากใส่อีเมลหรือรหัสผ่านผิดเกินจำนวนที่กำหนด เช่น 3 ครั้ง ทำการ block ip  5 นาที หรือ ตามเวลาที่กำหนด หรือ ทำการ block user ที่พยายามจะเข้าถึง
--> การใช้ 2 Factor Authen เข้ามาร่วมกับ การ Login ไม่ว่าจะเป็นการใช้ email confirm, otp หรืออื่นๆเข้าร่วม


2. จงเขียนตัวอย่าง sql query ในโค้ด php โดยให้มีชุดคำสั่ง
ที่ช่วยป้องกัน sql injection (ตั้งชื่อตารางชื่อฟิลด์ด้วยตัวเอง
ได้เลย)
--> Folder "phpsqlinject" => สร้างโดย Pure Php
--> table และ field "SELECT * FROM products WHERE shop_id = '$escstr_shop_id'"
--> ใช้การ Validate Input type และ real_escape_string เพื่อตัดการใช้คำสั่งพิเศษหรือการใช้ จำลองการ query เช่น shop_id = 1=1


3. จงเขียน saI query ที่มี sub query ในตำแหน่งต่างๆ อย่าง
น้อยสองแบบ (ตั้งชื่อตารางชื่อฟิลด์ด้วยตัวเองได้เลย)

--> SELECT p.*,(SELECT name FROM shops s WHERE s.id = p.shop_id ) as 'ShopName'
FROM products p 
WHERE shop_id IN (SELECT id FROM shops WHERE name LIKE '%Pop%');

4. จากตาราง tb_product(id,name,status,shop_id) และ
tb_shop(id,name)
จงเขียน โค้ด select เพื่อแสดงสินค้าของร้าน ที่มีชื่อร้าน "rudy
shop"

--> SELECT p.*,s.name as 'ShopName' FROM products p
JOIN shops s ON p.shop_id = s.id
WHERE s.name LIKE '%rudy%';

5. เขียนคำสั่ง update สินค้าทุกตัวของร้าน "rudy shop" ให้
มี status='0'

--> UPDATE product p
JOIN shops s ON p.shop_id = s.id
SET p.status = 0
WHERE s.name LIKE '%POP%'

6. จงเขียน function ของ code sql เพื่อปรับรูปแบบการ select ข้อมูล ตามประเภทข้อมูลดังนี้เพื่อให้ได้ผลลัพธืดังตัวอย่าง
  type date ให้แสดงผลเป็น dd/mm/YYYY
  type float,double ให้แสดงผลเป็น x,xxx,xxx.xx 
  (สามารถเขียนได้มากกว่า 2 ข้อที่ยกตัวอย่าง)

--> SELECT x.created_at ,DATE_FORMAT(x.created_at , "%d%/%m%/%Y %T") as 'NewDate' FROM rudy_exam_db.products x

--> SELECT 
a.price,
FORMAT(a.price , 2) as 'NumberFormat1', 
a.discount ,
FORMAT(a.discount , 2) as 'NumberFormat2',
FORMAT((a.price - a.discount),2) as 'NetPrice'
FROM  rudy_exam_db.products  a

7. จงเขียน code function php ในการคำนวณผลลัพธ์ใบเสนอราคาโดยหัวข้อมีดังนี้
   ราคาสินค้ารวม = สามารถตั้งเองได้
   ส่วนลดรวม = สามารถตั้งเองได้
   ราคาสินค้าหลังส่วนลด
   ภาษีมูลค่าเพิ่ม 7 %
   ราคารวมสุทธิ

--> Folder "rudy-exam" => สร้างโดย Laravel + Vue 3
--> ในขั้นตอนการ Add Product

   (ถ้าใช้ framework zend laravel node.js จะพิจารณาเป็นพิเศษ)

Laravel Login
Username:user1test@admin.com
Password:asasas12!AA

Database
rudy_exam_db.sql
DB_DATABASE=rudy_exam_db
DB_USERNAME=rudy_exam_db
DB_PASSWORD=asasas12
