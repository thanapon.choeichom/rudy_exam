-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2023 at 07:57 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rudy_exam_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_05_13_061635_create_products_table', 2),
(6, '2023_05_13_062106_create_shops_table', 3),
(7, '2023_05_13_163337_add_image_products_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'MyApp', '09fbb4fcf250bcd5d9850fc43ece4e0cedeba4397e816ee40e1833e147086df6', '[\"*\"]', NULL, '2023-05-12 23:41:39', '2023-05-12 23:41:39'),
(2, 'App\\Models\\User', 1, 'MyApp', '67d51fbda2aaaf0f693a1a2e857daa9c2ffbf0d23c85fddafb439d4cfc881acd', '[\"*\"]', NULL, '2023-05-12 23:44:18', '2023-05-12 23:44:18'),
(3, 'App\\Models\\User', 1, 'MyApp', '6edf6358b2a50ba44eba02505b782cd6bb31bb80ca3eb0447f2286fd9e602ffd', '[\"*\"]', '2023-05-13 10:05:14', '2023-05-13 08:45:35', '2023-05-13 10:05:14'),
(4, 'App\\Models\\User', 1, 'MyApp', '94fa096fe6ef7dff844c25156168590c3d5f9ce41fd6540b1a360801e2a59623', '[\"*\"]', NULL, '2023-05-15 02:30:34', '2023-05-15 02:30:34'),
(5, 'App\\Models\\User', 1, 'MyApp', '799db0716cb70bb661d70c9f193edfff87e6d76702dc74eceeb55ff3edb9464d', '[\"*\"]', NULL, '2023-05-15 02:32:27', '2023-05-15 02:32:27'),
(6, 'App\\Models\\User', 1, 'MyApp', '81e073e1b07593388d74bbbd61a17ef6f37a4f3ea9ab169e2b387cedfea0eb0b', '[\"*\"]', NULL, '2023-05-15 02:45:18', '2023-05-15 02:45:18'),
(7, 'App\\Models\\User', 1, 'MyApp', '2dd76974a93fdfac369ba68bfb65db3a4c262ac2a820af86e1bc2194c5e591ed', '[\"*\"]', NULL, '2023-05-15 02:45:58', '2023-05-15 02:45:58'),
(8, 'App\\Models\\User', 1, 'MyApp', 'c0aa2ca9f258f2da0af1a68c72699fa89c87c80e0f0f5c461ccf2cdfe4d3164f', '[\"*\"]', NULL, '2023-05-15 02:47:09', '2023-05-15 02:47:09'),
(9, 'App\\Models\\User', 1, 'MyApp', '77b7fa09af3332794ff3845164ca629c02c1c0a9476d87875e5ec4c6bcf4730a', '[\"*\"]', NULL, '2023-05-15 02:47:31', '2023-05-15 02:47:31'),
(10, 'App\\Models\\User', 1, 'MyApp', '3aa3ccacb2983f4810a413153aa87ca5e1c613dc08e4eb9b6bde3c5de97d777c', '[\"*\"]', NULL, '2023-05-15 03:04:03', '2023-05-15 03:04:03'),
(11, 'App\\Models\\User', 1, 'MyApp', '4c847f0e1a5742ec57ba6a00833cfdbe09e39f4d2dd084a60a0ddc57df64b4c5', '[\"*\"]', NULL, '2023-05-15 03:04:32', '2023-05-15 03:04:32'),
(12, 'App\\Models\\User', 1, 'MyApp', '8b20c7eed75b3a8803b726ee5caed4e01b1eee6e34f9ca1cb3a1219f5196acc3', '[\"*\"]', NULL, '2023-05-15 03:05:03', '2023-05-15 03:05:03'),
(13, 'App\\Models\\User', 1, 'MyApp', '9926f127344e6305cc82d736eacb5e476d05465e44f3e0ecc35dc8ee3ccfc428', '[\"*\"]', NULL, '2023-05-15 03:05:14', '2023-05-15 03:05:14'),
(14, 'App\\Models\\User', 1, 'MyApp', '15f17997554adde68e48d9c05948861ea119f64144c81366b03a970788582733', '[\"*\"]', NULL, '2023-05-15 03:49:35', '2023-05-15 03:49:35'),
(15, 'App\\Models\\User', 1, 'MyApp', '43e68a5a59e07acd70991ddea3b8396d61752580d963abc760383dd49bb5882a', '[\"*\"]', NULL, '2023-05-15 03:50:10', '2023-05-15 03:50:10'),
(16, 'App\\Models\\User', 1, 'MyApp', 'ff2d8a3e165a2a68ab9afef37916e9b2488a098f06634c5032741f5cfc54654d', '[\"*\"]', NULL, '2023-05-15 03:54:31', '2023-05-15 03:54:31'),
(17, 'App\\Models\\User', 1, 'MyApp', '0372e312fdefde85393ac78dd8e2ed39fbc30ecdc8ff87ad39c92e78363fbfff', '[\"*\"]', NULL, '2023-05-15 20:33:49', '2023-05-15 20:33:49'),
(18, 'App\\Models\\User', 1, 'MyApp', '3172e64e64ec46ec2362c0a1d7585cc4c404609a5137a38eae7de49241ddd163', '[\"*\"]', NULL, '2023-05-15 23:51:10', '2023-05-15 23:51:10'),
(19, 'App\\Models\\User', 1, 'MyApp', '8491fc5f7b4bd7e9bd55de58abf525c226c1763bd4b8984a06f8255325540ee2', '[\"*\"]', NULL, '2023-05-16 00:31:22', '2023-05-16 00:31:22'),
(20, 'App\\Models\\User', 1, 'MyApp', '0049db659102a78d4fffc9e1d6e53c9d04a4e6aa8bd988f6efc6151eb93de549', '[\"*\"]', '2023-05-16 00:43:06', '2023-05-16 00:33:13', '2023-05-16 00:43:06'),
(21, 'App\\Models\\User', 1, 'MyApp', 'd1fd40cd751d3156df1b884980fca29ce19a3f35f1fb7aef535678c095991f7f', '[\"*\"]', NULL, '2023-05-16 03:14:43', '2023-05-16 03:14:43'),
(22, 'App\\Models\\User', 1, 'MyApp', 'c390fed9b9d9bdd9b8341793a8afa8b0bb3d4548ac010ceca62616da126478a7', '[\"*\"]', NULL, '2023-05-16 03:15:08', '2023-05-16 03:15:08'),
(23, 'App\\Models\\User', 1, 'MyApp', '1f4ce8d3324a2f9e92fc1505b2a1f57bff8f6ff6a66ef3ddc828dac7047d4fe7', '[\"*\"]', NULL, '2023-05-16 03:15:38', '2023-05-16 03:15:38'),
(24, 'App\\Models\\User', 1, 'MyApp', 'b840155a3d9c005bf3961c121c95de46387388dcbfb0e3ded75d29bd265158c7', '[\"*\"]', NULL, '2023-05-16 03:17:10', '2023-05-16 03:17:10'),
(25, 'App\\Models\\User', 1, 'MyApp', 'be19f9ac8a9a9da8d59963ee3467bcd69129a401ea759df187e0d12f25c41ce3', '[\"*\"]', NULL, '2023-05-16 03:18:23', '2023-05-16 03:18:23'),
(26, 'App\\Models\\User', 1, 'MyApp', '7ffa451e1cdd62890bb570ab7718903c6b2474eeb514751cc267fe1480a26baa', '[\"*\"]', NULL, '2023-05-16 03:18:57', '2023-05-16 03:18:57'),
(27, 'App\\Models\\User', 1, 'MyApp', '1c0cf0f1065d7a8ee286f03d41b1db818a93d3319820e18cc225506d8590f92c', '[\"*\"]', NULL, '2023-05-16 03:28:41', '2023-05-16 03:28:41'),
(28, 'App\\Models\\User', 3, 'MyApp', '5a9fb114366a72bb632fb1cd1ef98dd98e01caf2ecf7f637ac284f6f6159f850', '[\"*\"]', NULL, '2023-05-16 07:56:51', '2023-05-16 07:56:51'),
(29, 'App\\Models\\User', 4, 'MyApp', '5277fb424bf4964af6c9066a9c40129364a75c1c2a110219642c398726b82b00', '[\"*\"]', NULL, '2023-05-16 08:00:42', '2023-05-16 08:00:42'),
(30, 'App\\Models\\User', 5, 'MyApp', 'e8ef1c77fee6259b5b9927dfe22b851d655e98d1d5dec958533a8616c8f47fb5', '[\"*\"]', NULL, '2023-05-16 08:02:06', '2023-05-16 08:02:06'),
(31, 'App\\Models\\User', 1, 'MyApp', '58583fe0d0be4daf2986ddfdefe17c01810a827de5326249bdbf3215b31dd496', '[\"*\"]', NULL, '2023-05-17 08:10:16', '2023-05-17 08:10:16');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` text DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `vat` float DEFAULT NULL,
  `vatprice` float DEFAULT NULL,
  `totalprice` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `status`, `shop_id`, `created_at`, `updated_at`, `image`, `price`, `discount`, `vat`, `vatprice`, `totalprice`) VALUES
(1, 'Product 1', 0, 1, '2023-05-12 23:50:19', '2023-05-13 09:06:19', '', 10000.5, 120, NULL, NULL, NULL),
(2, 'Product 2', 0, 2, '2023-05-13 09:01:03', '2023-05-13 09:06:28', '', 1500, 50.5, NULL, NULL, NULL),
(3, 'Product 1', 0, 1, '2023-05-13 09:20:58', '2023-05-13 09:20:58', '', NULL, NULL, NULL, NULL, NULL),
(4, 'Product 3', 0, 1, '2023-05-13 10:05:06', '2023-05-13 10:05:06', 'thumbnail-buffalo_rising.jpg', NULL, NULL, NULL, NULL, NULL),
(5, 'Product 4', 0, 1, '2023-05-13 10:05:14', '2023-05-13 10:05:14', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Product 5', 1, 3, '2023-05-16 08:34:43', '2023-05-16 08:34:43', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Product 6', 0, 2, '2023-05-16 08:35:53', '2023-05-16 08:35:53', 'register.jpg', NULL, NULL, NULL, NULL, NULL),
(8, 'Product 7', 1, 1, '2023-05-17 08:55:38', '2023-05-17 08:55:38', NULL, 1600, 150, 7, 101.5, 1551.5),
(9, 'Product 8', 1, 1, '2023-05-17 08:58:46', '2023-05-17 08:58:46', NULL, 950, 125, 7, 57.75, 882.75);

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Rudy', '2023-05-12 23:50:19', '2023-05-12 23:50:19'),
(2, 'Pop', '2023-05-12 23:50:19', '2023-05-12 23:50:19'),
(3, 'Thanapon', '2023-05-12 23:50:19', '2023-05-12 23:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'user1test', 'user1test@admin.com', NULL, '$2y$10$7fFsm41.Zq8wWM7fyQSeh.DNA.EPRksr874Z4IGsswOPOpFGtOK3K', NULL, '2023-05-12 23:41:39', '2023-05-12 23:41:39'),
(3, 'user2test', 'user2test@admin.admin', NULL, '$2y$10$RsEnBlJS2r/tLAcVZdUj8eDQhvXqSAdldWxq2h1sB.RTDie9FXXeO', NULL, '2023-05-16 07:56:51', '2023-05-16 07:56:51'),
(4, 'user3', 'user3@admin.admin', NULL, '$2y$10$747QtNU8tGmI.wT6Ueg4cuWvN35wQUvRsIfzomY.FyObdQmdGvhlW', NULL, '2023-05-16 08:00:42', '2023-05-16 08:00:42'),
(5, 'user4', 'user4@admin.admin', NULL, '$2y$10$KD1kIsaicexahULbTo1fAO7rPN8KeKsVZX2iMWlDHsCNdQHyz39wy', NULL, '2023-05-16 08:02:06', '2023-05-16 08:02:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
